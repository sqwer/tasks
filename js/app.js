var start = JSON.parse(localStorage.getItem('key')) || [];
var startimp = JSON.parse(localStorage.getItem('key1')) || [];
var time = JSON.parse(localStorage.getItem('key2')) || [];
var valuetimeformat = JSON.parse(localStorage.getItem('dateformat')) || [];
var check = JSON.parse(localStorage.getItem('check')) || [];
var checknodate = JSON.parse(localStorage.getItem('checknodate')) || [];
var executiontime = JSON.parse(localStorage.getItem('execution')) || [];
var message;

// Компонент задача
class Task extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      edit: false,
      check: this.props.checked,
      checknodate: this.props.checkednodate
    }
  }
  edit = () => {
    this.setState ({edit: true})
  }
  remove = () => {
    this.props.deleteBlock (this.props.index)
  }
  save = () => {
  	if (this.state.checknodate) {
    	this.props.update (this.refs.newTxt.value, this.props.index, this.refs.impTxt.value, null);
    } else {
  		this.props.update (this.refs.newTxt.value, this.props.index, this.refs.impTxt.value, this.refs.timeTxt.value);
    }
    this.setState ({edit: false})
  }
  handleCheck = () => {
    this.setState({check: !this.state.check});
    this.props.updatecheck (!this.state.check, this.props.index);
  }
  handleChecknodate = () => {
  	this.setState({checknodate: !this.state.checknodate});
  	this.props.handleChecknodate(!this.state.checknodate, this.props.index);
  }
  tick() {
  	this.props.over (this.props.index);
  }
  // счетчик для отслеживания просроченных задач
  componentDidMount() {
  	this.interval = setInterval(() => this.tick(), 1000);
  }
  componentWillUnmount() {
    clearInterval(this.interval);
  }
  // Форма для задачи
  rendNorm = () => {
  if (this.state.check) {
      	message = 'Выполнена';
    	} else {
     		message = 'Не выполнена';
    	}	
    return (
      <div>
        <div className="tasktop">
          {this.props.children}
        </div>
        <div className="taskdown">
          <div className="checkbox">
            <input type="checkbox" onChange={this.handleCheck} defaultChecked={this.state.check}/>{message}
          </div>
          <button className="button2" onClick={this.edit}>Редактировать</button>
          <button className="button2" onClick={this.remove}>Удалить</button>
        </div>


      </div>
    );
 };
 // Форма для редактирования задачи
  rendEdit = () => {
  if (this.state.checknodate) {
  	 return (
      <div>
        <div className="taskdown">
          <textarea className="editname" ref="newTxt" defaultValue={this.props.defaulttask}></textarea>
          <select className="selectbox" ref="impTxt">
            <option selected value={this.props.defaultimp}>{this.props.defaultimp}</option>
            <option value="обычная">обычная</option>
            <option value="важная">важная</option>
            <option value="очень важная">очень важная</option>
          </select>
        </div>
            <input type="checkbox" onChange={this.handleChecknodate} defaultChecked={this.state.checknodate}/> Без даты
          <button className="button3" onClick={this.save}>Сохранить</button>
      </div>
    );
  } else {
  	var date = this.props.defaultdate;
  	if (date == null)
    {
    	var defaultdate = new Date();
      var year = defaultdate.getFullYear();
  		var month = ("0" + (defaultdate.getMonth()+1)).slice(-2);
  		var day = ("0" + (defaultdate.getDate()+1)).slice(-2);
  		var hours = ("0" + (defaultdate.getHours())).slice(-2);
  		var minutes = ("0" + (defaultdate.getMinutes())).slice(-2);
  		var valuetimeTxt = year+"-"+month+"-"+day+"T"+hours+":"+minutes;
      date = valuetimeTxt;
    };
    return (
      <div>
        <div className="taskdown">
          <textarea className="editname" ref="newTxt" defaultValue={this.props.defaulttask}></textarea>
          <select className="selectbox" ref="impTxt">
            <option selected value={this.props.defaultimp}>{this.props.defaultimp}</option>
            <option value="обычная">обычная</option>
            <option value="важная">важная</option>
            <option value="очень важная">очень важная</option>
          </select>
          <input className="datebox" ref="timeTxt" type="datetime-local" defaultValue={date}/>
        </div>
            <input type="checkbox" onChange={this.handleChecknodate} defaultChecked={this.state.checknodate}/> Без даты
          <button className="button3" onClick={this.save}>Сохранить</button>
      </div>
    );
    }
  };

  render() {
    if (this.state.edit) {
      return this.rendEdit ();
    } else {
      return this.rendNorm ();
    }
  }
}
// Родительский компонент для размещения задач
class Field extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tasks: start.map(x => x),
      importance: startimp.map(x => x),
      endtime: time.map(x => x),
      valuetime: valuetimeformat.map(x => x),
      checked: check.map(x => x),
      checkednodate: checknodate.map(x =>x),
      execution: executiontime.map(x => x),
      condition: [null, null, null],
      color: []
    };
  };
  // Функция ослеживания просроченных задач
  over = (i) => {
  var datenow = new Date();
  var year = datenow.getFullYear();
  var month = ("0" + (datenow.getMonth())).slice(-2);
  var day = ("0" + (datenow.getDate())).slice(-2);
 	var hours = ("0" + (datenow.getHours())).slice(-2);
  var minutes = ("0" + (datenow.getMinutes())).slice(-2);
  var endtime = this.state.endtime;
  var timein = this.state.endtime[i];
  var yearin = timein.slice(6,10);
  var monthin = timein.slice(3,5)-1;
  var dayin = timein.slice(0,2);
  var hoursin = timein.slice(11,13);
  var minutesin = timein.slice(14,16);
  var versusdate = new Date (year, month, day, hours, minutes);
  var versusdatein = new Date (yearin, monthin, dayin, hoursin, minutesin);
  if (versusdate >= versusdatein && this.state.execution[i] == null)
  {
  	var color = this.state.color;
    color[i] = {background: '#e91e63'};
    this.setState ({color:color});
  } else { 
  	var color = this.state.color;
    color[i] = {background: '#f5f5f5'};
    this.setState ({color:color});
    }
  }
  // Функция для установления времени завершения задачи
  updatecheck = (updatecheck, i) => {
  var check = this.state.checked;
  check[i] = updatecheck;
  this.setState ({checked:check});
  if (this.state.checked[i] == true) {
      	var executiontime = this.state.execution;
  			var executiondate = new Date();
  			var year = executiondate.getFullYear();
  			var month = ("0" + (executiondate.getMonth()+1)).slice(-2);
  			var day = ("0" + (executiondate.getDate())).slice(-2);
 				var hours = ("0" + (executiondate.getHours())).slice(-2);
  			var minutes = ("0" + (executiondate.getMinutes())).slice(-2);
  			var time = day+"."+month+"."+year+" "+hours+":"+minutes;
  			executiontime[i] = time;
  			this.setState ({execution:executiontime});
        localStorage.setItem('execution', JSON.stringify(executiontime));
    	} else {
      	var executiontime = this.state.execution;
        executiontime[i] = null;
      	this.setState ({execution:executiontime});
        localStorage.setItem('execution', JSON.stringify(executiontime));
      }
  localStorage.setItem('check', JSON.stringify(check));
  }
  // Добавление новой задачи
  add = (text, seltext, endtime, check) => {
  var arr = this.state.tasks;
  var imp = this.state.importance;
  var date = this.state.endtime;
  var valuet = this.state.valuetime;
  var checkstate = this.state.checked;
  arr.push (text);
  imp.push (seltext);
  checkstate.push (check);
  var year = endtime.getFullYear();
  var month = ("0" + (endtime.getMonth()+1)).slice(-2);
  var day = ("0" + (endtime.getDate()+1)).slice(-2);
  var hours = ("0" + (endtime.getHours())).slice(-2);
  var minutes = ("0" + (endtime.getMinutes())).slice(-2);
  var valuetimeTxt = year+"-"+month+"-"+day+"T"+hours+":"+minutes;
  var time = day+"."+month+"."+year+" "+hours+":"+minutes;
  date.push (time);
  valuet.push (valuetimeTxt);
  this.setState ({tasks:arr});
  this.setState ({importance:imp});
  this.setState ({endtime:date});
  this.setState ({valuetime:valuet});
  this.setState ({checked:checkstate});
  localStorage.setItem('key', JSON.stringify(arr));
  localStorage.setItem('key1', JSON.stringify(imp));
  localStorage.setItem('key2', JSON.stringify(date));
  localStorage.setItem('dateformat', JSON.stringify(valuet));
  localStorage.setItem('check', JSON.stringify(checkstate));
  }
  // Удаление задачи
  deleteBlock = (i) => {
    var arr = this.state.tasks;
    var imp = this.state.importance;
    var date = this.state.endtime;
    var valuet = this.state.valuetime;
    var checkstate = this.state.checked;
    var executionstate = this.state.execution;
    arr.splice (i, 1);
    imp.splice (i, 1);
    date.splice (i, 1);
    valuet.splice (i, 1);
    checkstate.splice (i, 1);
    executionstate.splice(i, 1);
    this.setState ({tasks: arr});
    this.setState ({importance:imp});
    this.setState ({endtime:date});
    this.setState ({valuetime:valuet});
    this.setState ({checked:checkstate});
    this.setState ({execution:executionstate});
    localStorage.setItem('key', JSON.stringify(arr));
    localStorage.setItem('key1', JSON.stringify(imp));
    localStorage.setItem('key2', JSON.stringify(date));
    localStorage.setItem('dateformat', JSON.stringify(valuet));
    localStorage.setItem('check', JSON.stringify(checkstate));
    localStorage.setItem('execution', JSON.stringify(executionstate));
  }
  // Редактирование задачи
	updateText = (text, i, seltext, valuetime) => {
    var arr = this.state.tasks;
    var imp = this.state.importance;
    var date = this.state.endtime;
    var valuet = this.state.valuetime;
    arr[i] = text;
    imp[i] = seltext;
     if (this.state.checkednodate[i] == true) {
      			date[i] = null;
    				valuet[i] = null;
            var color = this.state.color;
    				color[i] = {background: '#f5f5f5'};
    				this.setState ({color:color});
    	} else {
      	    var year = valuetime.slice(0,4);
  					var month = valuetime.slice(5,7);
  					var day = valuetime.slice(8,10);
  					var hours = valuetime.slice(11,13);
  					var minutes = valuetime.slice(14,16);
    				var time = day+"."+month+"."+year+" "+hours+":"+minutes;
    				date[i] = time;
    				valuet[i] = valuetime;
      }
    this.setState ({tasks: arr});
    this.setState ({importance:imp});
    this.setState ({endtime:date});
    this.setState ({valuetime:valuet});
    localStorage.setItem('key', JSON.stringify(arr));
    localStorage.setItem('key1', JSON.stringify(imp));
    localStorage.setItem('key2', JSON.stringify(date));
    localStorage.setItem('dateformat', JSON.stringify(valuet));
  }
  handleChecknodate = (updatecheck,i) => {
  var check = this.state.checkednodate;
  check[i] = updatecheck;
  }
  // Вид отображения задачи
  eachTask = (item, i) => {
      return (
    	<div className="task" style={this.state.color[i]}>
        <Task key={i} index={i} over={this.over} checkednodate={this.state.checkednodate[i]} handleChecknodate={this.handleChecknodate} checked={this.state.checked[i]} updatecheck={this.updatecheck} update={this.updateText} deleteBlock={this.deleteBlock} defaulttask={this.state.tasks[i]} defaultimp={this.state.importance[i]} importance={this.state.importance} endtime={this.state.endtime} defaultdate={this.state.valuetime[i]}>
          {this.state.tasks[i]}
          {' '}
          {this.state.importance[i]}
          {' '} 
          {this.state.endtime[i]}
          {' '}
          {this.state.execution[i]}
        </Task>
      </div>
    );
  }
  // Фильтр задач
   eachImportance = (item, i) => {
   var input = this.state.condition;
         if (input[0] == null && input[1] == null && input[2] == null) {   
    return (
				this.eachTask(item,i)
        )
   		} 
   if (item == input[0] || item == input[1] || item == input[2]) {
    return (
    this.eachTask(item,i)
 		)
    } 
    if (input[0] != "обычная" && input[1] != "важная" && input[2] != "очень важная") {   
    return (
    this.eachTask(item,i)
    )
   		}
  };
  // обычные задачи
  handleChecknormal = () =>
  {
  	var input = this.state.condition;
  	if (this.refs.checknorm.value == "обычная") {
    		this.refs.checknorm.value = null
    } else {
				this.refs.checknorm.value = "обычная"
    }
    input[0] = this.refs.checknorm.value;
    this.setState({condition: input});
  }
  // важные задачи
  handleCheckimportant = () =>
  {
  	var input = this.state.condition;
  	if (this.refs.checknorm1.value == "важная") {
    		this.refs.checknorm1.value = null
    } else {
				this.refs.checknorm1.value = "важная"
    }
    input[1] = this.refs.checknorm1.value;
    this.setState({condition: input});
  }
  // очень важные задачи
    handleCheckveryimportant = () =>
  {
  	var input = this.state.condition;
  	if (this.refs.checknorm2.value == "очень важная") {
    		this.refs.checknorm2.value = null
    } else {
				this.refs.checknorm2.value = "очень важная"
    }
    input[2] = this.refs.checknorm2.value;
    this.setState({condition: input});
  }
  render() {
    return (
      <div className="field">
      <div className="buttondiv1">
        <button className="button1" onClick={this.add.bind (null,'Имя задачи','обычная',new Date(),false)}>НОВАЯ ЗАДАЧА</button>
      </div>
        <div className="filter">
          <div className="filtertop">Важность</div>
          <div className="filterdown">
              <input className="filtercheck" ref="checknorm" type="checkbox" onChange={this.handleChecknormal}/>
              <input className="filtercheck" ref="checknorm1" type="checkbox" onChange={this.handleCheckimportant}/>
              <input className="filtercheck" ref="checknorm2" type="checkbox" onChange={this.handleCheckveryimportant}/>
          </div>
          <div className="filterdown">
            <h className="filterlable">обычная</h>
            <h className="filterlable">важная</h>
            <h className="filterlable">очень важная</h>
          </div>
        </div>
        {this.state.importance.map(this.eachImportance)}
      </div>
    );
  }
}
ReactDOM.render(
	<div>
    <Field/>
  </div>, root);